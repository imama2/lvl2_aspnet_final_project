//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LVL2_ASPNet_Final_Project.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class tbl_akun
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_akun()
        {
            this.tbl_Ortu = new HashSet<tbl_Ortu>();
            this.tbl_error_log_history = new HashSet<tbl_error_log_history>();
        }
    
        public int id { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string password_login { get; set; }
        [Required]
        //[DataType(DataType.EmailAddress,ErrorMessage ="Invalid Email Address")]
        public string email { get; set; }
        public string PhoneNumber { get; set; }
        public string Verify_Status { get; set; }
        public string Account_Type { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Ortu> tbl_Ortu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_error_log_history> tbl_error_log_history { get; set; }
    }
}
