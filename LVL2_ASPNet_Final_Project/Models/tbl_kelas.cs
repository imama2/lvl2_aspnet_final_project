//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LVL2_ASPNet_Final_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_kelas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_kelas()
        {
            this.tbl_kegiatan = new HashSet<tbl_kegiatan>();
            this.tbl_siswa = new HashSet<tbl_siswa>();
        }
    
        public int id { get; set; }
        public string nama_kelas { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_kegiatan> tbl_kegiatan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_siswa> tbl_siswa { get; set; }
    }
}
