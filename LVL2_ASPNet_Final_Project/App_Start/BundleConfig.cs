﻿using System.Web;
using System.Web.Optimization;

namespace LVL2_ASPNet_Final_Project
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/additional-methods.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js");


            //bundles.Add(new StyleBundle("~/bundles/css").Include(
            //          "~/Content/external/css/*.css", // Just copy all of them for now, we'll select some of them after final.
            //          "~/Content/external/fonts/*.css")); 

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/external/fonts/fontawesome5-overrides.min.css",
                "~/Content/external/css/styles.min.css"));

            bundles.Add(new StyleBundle("~/bundles/poppers").Include(
                "~/Scripts/umd/popper.js",
                "~/Scripts/umd/popper-utils.js"));

            bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include(
                "~/Content/bootstrap.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                "~/Scripts/bootstrap.bundle.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryplugins").Include(
                      "~/Scripts/jquery.ba-throttle-debounce.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                      "~/Scripts/script.min.js"));
        }
    }
}
