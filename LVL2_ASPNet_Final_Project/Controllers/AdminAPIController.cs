﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LVL2_ASPNet_Final_Project.Models;

namespace LVL2_ASPNet_Final_Project.Controllers
{
    public class AdminAPIController : ApiController
    {
        kindergarten_dbEntities db = new kindergarten_dbEntities();

        [HttpGet]
        public string test()
        {
            return "ye";
        }

        [HttpGet]
        public HttpResponseMessage checkIndividualExist(string name)
        {
            if (db.tbl_family.Where(x => x.child_name.ToLower() == name.ToLower()).FirstOrDefault() != null)
                return Request.CreateResponse(HttpStatusCode.OK);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpGet]
        public HttpResponseMessage checkClassExist(string name)
        {
            if (db.tbl_class.Where(x => x.className.ToLower() == name.ToLower()).FirstOrDefault() != null)
                return Request.CreateResponse(HttpStatusCode.OK);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpGet] 
        public HttpResponseMessage getUserDataCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, db.view_userDataList.Count());
        }

        [HttpGet] // This query shouldn't be used on production and should only be used on debug. Disable on production.
        public HttpResponseMessage getUserDataList()
        {
            return Request.CreateResponse(HttpStatusCode.OK, db.view_userDataList.OrderBy(x => x.id).ToList());
        }

        [HttpGet]
        public HttpResponseMessage getUserDataList(int offset, int limit)
        {
            return Request.CreateResponse(HttpStatusCode.OK, db.view_userDataList.OrderBy(x => x.id).Skip(offset).Take(limit).ToList());
        }

        [HttpGet]
        public HttpResponseMessage getUserDataDetails(int id)
        {
            var userData = db.view_userDataDetails.Where(x => x.id == id).ToList().FirstOrDefault();
            if (userData != null)
                return Request.CreateResponse(HttpStatusCode.OK, userData);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}
