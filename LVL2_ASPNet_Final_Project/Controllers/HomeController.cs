﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient; //this namespace is for sqlclient server  
using System.Configuration;
using LVL2_ASPNet_Final_Project.Models;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_Final_Project.Controllers
{
    public class HomeController : Controller
    {
        db_TK_DzakiraEntities Db = new db_TK_DzakiraEntities();
       
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]  
        public ActionResult Login(LVL2_ASPNet_Final_Project.Models.tbl_akun userModel)   
        {
            var userDetails = Db.tbl_akun.Where(x => x.email == userModel.email && x.password_login == userModel.password_login).FirstOrDefault();
            if (userDetails == null)
            {
             return View();
            }
            else
            {
            Session["userID"] = userDetails.id;
            Session["userName"] = userDetails.email;
            return RedirectToAction("Index", "User");
            }
        }
        public ActionResult LogOut()
        {
            int userId = (int)Session["userID"];
            Session.Abandon();
            return RedirectToAction("Index","User");
        }
        public ActionResult UserDashBoard()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index","User");
            }
        }
    }
}