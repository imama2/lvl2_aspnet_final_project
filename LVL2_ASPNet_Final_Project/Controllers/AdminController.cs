﻿using LVL2_ASPNet_Final_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_Final_Project.Controllers
{
    public class AdminController : Controller
    {
        kindergarten_dbEntities db = new kindergarten_dbEntities();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Verification()
        {
            return View();
        }

        public ActionResult Evaluation()
        {
            return View();
        }

        public ActionResult DailySchedule()
        {
            return View();
        }

        public ActionResult DailyScheduleEdit()
        {
            return View();
        }

        public ActionResult UserFormData()
        {
            return View();
        }

        public ActionResult UserFormDataDetails(int id)
        {
            var userData = db.view_userDataDetails.Where(x => x.id == id).ToList().FirstOrDefault();
            if (userData != null)
                return View(userData);
            else
                return new HttpNotFoundResult();
        }
    }
}